﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public AnyGameObject[] gameobjects;
    public GameObject gameobject;
    public GameObject canvas;
    public List<GameObject> gameobjectList;

    void Start()
    {
        for (var i = 0; i < gameobjects.Length; i++)
        {
            gameobjectList.Add(Instantiate(gameobject, canvas.transform));                  // Instantiating the buttons
            var go = gameobjectList[i].transform.Find("Button").GetComponent<Button>();     // Retrieve subitem button
            go.onClick.AddListener(delegate { OnClick(i); });                               // Add ClickEvent on subitem button
            go.transform.GetComponentInChildren<TextMeshProUGUI>().text = "Button " + i;    // Reposition so the buttons won't overlap
            SetPosition(i);
        }
    }

    // This click has to be triggered by each subitem of 'AnyGameObject'
    private void SetPosition(int i)
    {
        Vector3 position = gameobjectList[i].transform.position;
        position.x += 300 * i;
        gameobjectList[i].transform.position = position;
    }

    public void OnClick(int i)
    {
        Debug.Log("Button " + i + " was clicked.");
    }
}
