﻿using UnityEngine;

[CreateAssetMenu(fileName = "AnyGameObject", menuName = "Game/AnyGameObject")]
public class AnyGameObject : ScriptableObject
{
    public string name;
}
